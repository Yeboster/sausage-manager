# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# User creation
User.create(username: 'salsiccia', password: 'salsiccia')

# Food creation

food_list = [
  ['Gnocchi S. Ghitan', 'primary', 5],
  ['Gnocchi de Momi', 'primary', 5],
  ['Gnocchi burro e salvia', 'primary', 5],
  ['Gnocchi ragù', 'primary', 5],
  ['Gnocchi 4 formaggi e noci', 'primary', 5],
  ['Gnocchi "del dì"', 'primary', 5],
  ['Piatto Sagra polenta, crauti e fagioli', 'primary', 9],
  ['Piatto Sagra, polenta, patate fritte', 'primary', 9],
  ['Salsicce, polenta, crauti e fagioli', 'primary', 8],
  ['Salsicce, polenta, patate fritte', 'primary', 8],
  ['Costine, polenta, crauti e fagioli', 'primary', 8],
  ['Costine, polenta, patate fritte', 'primary', 8],
  ['Wurstel, polenta, crauti e fagioli', 'primary', 7],
  ['Wurstel, polenta, patate fritte', 'primary', 7],
  ['1/2 Galletto, polenta, crauti e fagioli', 'primary', 9],
  ['1/2 Galletto, polenta, patatine', 'primary', 9],
  ['Soppressa, form., crauti e fagioli', 'primary', 7],
  ['Soppressa, form., patate fritte', 'primary', 7],
  ['Patatine fritte', 'primary', 2],
  ['Crauti, fagioli', 'primary', 2]
]

food_list.each do |name, color, price|
  Product.create(name: name, color: color, price: price) # The category is by default food
end

# Drinks creation

drink_list = [
  ['1 Lt. di Coca', 'primary', 3],
  ['Spritz', 'primary', 1.5],
  ['Spritz Aperol', 'primary', 2],
  ['Litro Spritz', 'primary', 7],
  ['Bicchiere di Coca', 'primary', 1],
  ['Bicchiere di Bianco', 'primary', 1],
  ['1/2 Lt. di vino Bianco', 'primary', 3],
  ['1 Lt. di vino Bianco', 'primary', 5],
  ['Bicchiere di Rosso', 'primary', 1],
  ['1/2 Lt. di vino Rosso', 'primary', 3],
  ['1 Lt. di vino Rosso', 'primary', 5],
  ['Bicchiere di Rabosello', 'primary', 1],
  ['1/2 Lt. di vino Rabosello', 'primary', 3],
  ['1 Lt. di vino Rabosello', 'primary', 5],
  ['Birra bionda 0,25', 'primary', 2],
  ['Birra bionda 0,40', 'primary', 3.5],
  ['Birra bionda 1 Lt.', 'primary', 7],
  ['Birra radler 0,25', 'primary', 2],
  ['Birra radler 0,40', 'primary', 3.5],
  ['Birra radler 1 Lt.', 'primary', 7],
  ['Birra Special/Rossa 0,25', 'primary', 3],
  ['Birra Special/Rossa 0,40', 'primary', 5],
  ['Birra Special/Rossa 1 Lt.', 'primary', 10],
  ['1/2 Lt. acqua naturale', 'primary', 1],
  ['1/2 Lt. acqua gasata', 'primary', 1],
  ['Lattina', 'primary', 2]
]

drink_list.each do |name, color, price|
  Product.create(name: name, color: color, price: price, category: 'drink')
end
