# frozen_string_literal: true

class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.float :total, null: false
      t.integer :user_id, null: false
      t.string :food_receipt_path
      t.string :drink_receipt_path
      t.string :customer_receipt_path

      t.timestamps
    end
    add_foreign_key :orders, :users
  end
end
