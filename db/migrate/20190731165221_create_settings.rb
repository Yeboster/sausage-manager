class CreateSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :settings do |t|
      t.string :food_printer
      t.string :drink_printer
      t.string :customer_printer
      t.boolean :using_esc

      t.timestamps
    end
  end
end
