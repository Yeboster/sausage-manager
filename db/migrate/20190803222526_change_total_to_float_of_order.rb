# frozen_string_literal: true

class ChangeTotalToFloatOfOrder < ActiveRecord::Migration[5.2]
  def change
    change_column :orders, :total, :decimal,
                  precision: 8, scale: 2,
                  default: 0, null: false
  end
end
