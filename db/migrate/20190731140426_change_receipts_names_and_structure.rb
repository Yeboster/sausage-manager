# frozen_string_literal: true

class ChangeReceiptsNamesAndStructure < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :customer_receipt_esc, :text
    rename_column :orders, :customer_receipt_path, :customer_receipt_pdf
    rename_column :orders, :drink_receipt_path, :drink_receipt_pdf
    rename_column :orders, :food_receipt_path, :food_receipt_pdf
  end
end
