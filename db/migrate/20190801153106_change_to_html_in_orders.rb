# frozen_string_literal: true

class ChangeToHtmlInOrders < ActiveRecord::Migration[5.2]
  def change
    rename_column :orders, :food_receipt_pdf, :food_receipt
    rename_column :orders, :drink_receipt_pdf, :drink_receipt
    rename_column :orders, :customer_receipt_pdf, :customer_receipt
  end
end
