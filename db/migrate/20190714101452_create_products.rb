# frozen_string_literal: true

class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.text :description
      t.float :price, null: false
      t.string :status, default: 'available'
      t.string :category, default: 'food'

      t.timestamps
    end

    add_index :products, :name
  end
end
