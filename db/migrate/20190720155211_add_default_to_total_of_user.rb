class AddDefaultToTotalOfUser < ActiveRecord::Migration[5.2]
  def change
    change_column :orders, :total, :integer, default: 0
  end
end
