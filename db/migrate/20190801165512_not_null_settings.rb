# frozen_string_literal: true

class NotNullSettings < ActiveRecord::Migration[5.2]
  def change
    change_column :settings, :food_printer, :text, null: false
    change_column :settings, :drink_printer, :text, null: false
    change_column :settings, :customer_printer, :text, null: false
    change_column :settings, :using_esc, :boolean, default: true
  end
end
