# frozen_string_literal: true

class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.integer :quantity, null: false, default: 1
      t.integer :order_id, null: false
      t.integer :product_id, null: false

      t.timestamps
    end
    add_foreign_key  :order_items, :orders
    add_foreign_key  :order_items, :products
  end
end
