# frozen_string_literal: true

Rails.application.routes.draw do
  resources :settings
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users,
             controllers: { registrations: 'registrations' }

  root 'orders#new'

  resource :order do
    member do
      get :all
      patch :add_product
      patch :remove_product
      post :reset
      post :save
      post :add_note
    end
  end

  resource :setting do
    member do
      get :index
      patch :update, as: :update_current
    end
  end
end
