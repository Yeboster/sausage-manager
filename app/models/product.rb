# frozen_string_literal: true

# Product model
class Product < ApplicationRecord
  has_many :order_items
  has_many :orders, through: :order_items

  attr_accessor :order_quantity

  def drink?
    category == 'drink'
  end

  def food?
    category == 'food'
  end

  def self.cock_ids
    [15, 16]
  end

  def self.sausages_ids
    [9, 10]
  end
end
