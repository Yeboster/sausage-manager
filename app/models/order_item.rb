# frozen_string_literal: true

# Model to identify the quantity of a product inside the order
class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :product

  def self.cocks_sold(_date = DateTime.now.beginning_of_day)
    qty = 0

    all.pluck(:product_id, :quantity).each do |p|
      qty += p[1] if Product.cock_ids.include? p[0]
    end

    qty
  end

  def self.sausages_sold(_date = DateTime.now.beginning_of_day)
    qty = 0

    all.pluck(:product_id, :quantity).each do |p|
      qty += p[1] if Product.sausages_ids.include? p[0]
    end

    qty
  end
end
