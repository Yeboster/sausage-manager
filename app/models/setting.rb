# frozen_string_literal: true

class Setting < ApplicationRecord
  def self.current
    Setting.first
  end

  def self.valid?
    current.nil?
  end

  def self.printers
    CupsPrinter.get_all_printer_names
  end

  def self.cups_food_printer
    CupsPrinter.new current.food_printer
  end

  def self.cups_drink_printer
    CupsPrinter.new current.drink_printer
  end

  def self.cups_customer_printer
    CupsPrinter.new current.customer_printer
  end

  def self.using_esc?
    current.using_esc
  end
end
