# frozen_string_literal: true

# Order model
class Order < ApplicationRecord
  belongs_to :user
  has_many :order_items
  has_many :products, through: :order_items

  default_scope { order(created_at: :desc) }

  def products_with_quantity
    list = []

    order_items.each do |item|
      qty = item.quantity
      product = item.product
      product.order_quantity = qty

      list.append product
    end

    list
  end

  def quantity_of(product)
    order_items.where(product_id: product&.id).first.quantity
  rescue StandardError
    0
  end

  def foods_with_quantity
    products_with_quantity.map { |item| item if item.food? }.compact
  end

  def drinks_with_quantity
    products_with_quantity.map { |item| item if item.drink? }.compact
  end

  def drinks
    products.map { |p| p if p.drink? }
            .compact
  end

  def foods
    products.map { |p| p if p.food? }
            .compact
  end

  def add_product(product)
    return unless product&.is_a? Product

    order_item = order_items.where(product_id: product.id).first
    if order_item
      order_item.quantity += 1
      order_item.save!
    else
      products << product
    end

    self.total += product.price
    save!
  end

  def remove_product(product)
    return unless product.is_a? Product

    order_item = order_items.where(product_id: product.id).first

    return unless order_item

    if order_item.quantity > 1
      order_item.quantity -= 1
      order_item.save!
    else
      order_item.destroy!
    end
    self.total -= product.price
    save!
  end

  def print_all
    print_food
    print_drink
    print_customer
  end

  def print_food
    return if food_receipt.nil? || food_receipt.empty?

    food_printer = Setting.cups_food_printer
    food_pdf = generate_pdf food_receipt

    print_pdf_a5 food_printer, food_pdf
  end

  def print_drink
    return if drink_receipt.nil? || drink_receipt.empty?

    drink_printer = Setting.cups_drink_printer
    drink_pdf = generate_pdf drink_receipt

    print_pdf_a5 drink_printer, drink_pdf
  end

  def print_customer
    customer_printer = Setting.cups_customer_printer

    if Setting.using_esc?
      customer_esc = Base64.decode64 customer_receipt_esc
      # TODO: Find a way to print with the pos printer
    else
      customer_pdf = generate_pdf customer_receipt
      print_pdf_a5 customer_printer, customer_pdf
    end
  end

  private

  def generate_pdf(content)
    WickedPdf.new.pdf_from_string(content)
  end

  def print_pdf_a5(printer, content)
    printer.print_data content, 'application/pdf', 'PageSize': 'A5'
  end
end
