# frozen_string_literal: true

class OrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :current_settings_valid
  before_action :current_order
  before_action :relevant_sold_products

  def new
    @foods = Product.where category: 'food'
    @drinks = Product.where category: 'drink'
  end

  def all; end

  # Add product or increment quantity
  def add_product
    @product = Product.find(permitted_params[:id])
    @current_order.add_product @product

    @product.order_quantity = @current_order.quantity_of @product

    respond_to do |format|
      format.js
    end
  end

  def remove_product
    @product = Product.find(permitted_params[:id])
    @current_order.remove_product @product

    @product.order_quantity = @current_order.quantity_of @product

    respond_to do |format|
      format.js
    end
  end

  def add_note
    note = params[:order][:note]
    error = t('errors.blank') if note.empty?

    respond_to do |format|
      format.js do
        if error
          render js: "error = document.getElementById('modalError');
          if(error != null){
          error.innerHTML = '#{t('errors.blank')}';
          }"
        else
          @current_order.update note: note.html_safe
          flash[:success] = t('orders.note.saved')
          redirect_to :root
        end
      end
    end
  end

  def save
    if @current_order.products&.size&.positive?
      generate_receipts
      print
      @current_order.completed = true
      @current_order.save!

      flash[:success] = t('orders.saved')
    else
      flash[:danger] = t('orders.empty')
    end

    redirect_to :root
  end

  def print(order = @current_order)
    order.print_all
  end

  private

  def permitted_params
    params.permit(:id)
  end

  def current_order
    user_id = current_user.id
    orders_not_completed = Order.where(user_id: user_id, completed: false)

    @current_order = if orders_not_completed.empty?
                       Order.create user_id: user_id, completed: false
                     else
                       orders_not_completed.first
                     end
  end

  def generate_receipts(order = @current_order)
    unless (foods = order.foods_with_quantity).empty?
      order.food_receipt = render_to_string partial: 'staff_receipt',
                                            locals: { order_id: order.id,
                                                      product_type: t('foods.name'),
                                                      items: foods }
    end

    unless (drinks = order.drinks_with_quantity).empty?
      order.drink_receipt = render_to_string partial: 'staff_receipt',
                                             locals: { order_id: order.id,
                                                       product_type: t('drinks.name'),
                                                       items: drinks }
    end

    order.customer_receipt_esc = render_to_string partial: 'customer_receipt_esc',
                                                  locals: { pos_view: Escpos::Printer.new,
                                                            order_id: order.id,
                                                            items: order.products_with_quantity }

    order.customer_receipt = render_to_string partial: 'customer_receipt',
                                              locals: { order_id: order.id,
                                                        items: order.products_with_quantity }
    order.save!
  end

  def relevant_sold_products
    @relevant_products = [
      { name: t('products.cocks_sold'),
        quantity: OrderItem.cocks_sold },
      { name: t('products.sausages_sold'),
        quantity: OrderItem.sausages_sold }
    ]
  end

  def current_settings_valid
    redirect_to :settings, notice: 'Seleziona le stampanti da utilizzare e salva' if Setting.valid?
  end
end
