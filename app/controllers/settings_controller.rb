# frozen_string_literal: true

class SettingsController < ApplicationController
  before_action :authenticate_user!
  before_action :current_settings

  def index
    @printers = Setting.printers
  end

  def update
    if @settings.update(setting_params)
      redirect_to settings_path, notice: 'Salvato correttamente'
    else
      render :index
    end
  end

  private

  def current_settings
    @settings = if Setting.valid?
                  Setting.new
                else
                  Setting.current
                end
  end

  # Only allow a trusted parameter "white list" through.
  def setting_params
    params.require(:setting)
          .permit(:food_printer, :drink_printer, :customer_printer, :using_esc)
  end
end
